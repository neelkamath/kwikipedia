# KWikipedia

[![Download](https://api.bintray.com/packages/neelkamath/kwikipedia/kwikipedia/images/download.svg)](https://bintray.com/neelkamath/kwikipedia/kwikipedia/_latestVersion)

This is a minimalist Kotlin Wikipedia library (pronounced "Quickipedia") which deals with the inconsistencies in Wikipedia's API by performing tasks such as data sanitization for you.

There are [GitHub releases](https://github.com/neelkamath/kwikipedia/releases) and a [Changelog](docs/CHANGELOG.md).

## Installation

See `Maven build settings` on [Bintray](https://bintray.com/neelkamath/kwikipedia/kwikipedia)

## [Usage](https://neelkamath.gitlab.io/kwikipedia/)

## [Contributing](docs/CONTRIBUTING.md)

## License

This project is under the [MIT License](LICENSE).
